for repoURI in $(cat ./pythonPackagesToInstallFromGit.txt); do
  git clone --depth=50 $repoURI PKG_DIR;
  cd ./PKG_DIR;
  python3 -m build -nwx .;
  pip3 install --upgrade --pre --no-deps ./dist/*.whl;
  cd ..;
  rm -rf ./PKG_DIR;
done;
