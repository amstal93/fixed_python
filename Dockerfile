# syntax=docker/dockerfile:experimental
FROM registry.gitlab.com/kolanich-subgroups/docker-images/cpp:latest
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./*.sh ./
ADD ./.ci/pythonPackagesToInstallFromGit.txt ./
ADD ./fix_compiler.py ./
ADD ./fix_python_modules_paths.py ./
ADD ./pip.conf ./
# todo: pypy
RUN \
	set -ex;\
	mv ./pip.conf /etc/;\
	./pythonStdlibFixes.sh ;\
	apt-get update ;\
	apt-get install -y python3 python3-distutils libpython3-dev lsb-release python-is-python3;\
	python3 fix_compiler.py;\
 \
	git clone --depth=50 https://github.com/pypa/setuptools.git;\
	git clone --depth=50 https://github.com/pypa/setuptools_scm.git;\
	git clone --depth=50 https://github.com/pypa/pip.git;\
	git clone --depth=50 https://github.com/pypa/wheel.git;\
	export SUDO=;\
	cd ./setuptools;\
	python3 setup.py egg_info;\
	cd ../wheel;\
	python3 -c "import re;from pathlib import Path;f=Path('setup.cfg');f.write_text(re.subn('^install_requires.+$', '', f.read_text(), flags=re.MULTILINE)[0])";\
	PYTHONPATH=../setuptools:./src $SUDO python3 ./setup.py install;\
	cd ../setuptools;\
	python3 ./setup.py install;\
	python3 ./setup.py bdist_wheel;\
	PYTHONPATH=../pip/src $SUDO python3 -m pip install --upgrade --force-reinstall ./dist/*.whl;\
	cd ../wheel;\
	PYTHONPATH=../setuptools python3 ./setup.py bdist_wheel;\
	PYTHONPATH=../pip/src $SUDO python3 -m pip install --upgrade --force-reinstall ./dist/*.whl;\
	cd ../pip;\
	python3 ./setup.py bdist_wheel;\
	PYTHONPATH=./src $SUDO python3 -m pip install --upgrade --force-reinstall ./dist/*.whl;\
	cd ..;\
	rm -rf ./setuptools ./pip ./wheel;\
	git clone --depth=50 https://github.com/uiri/toml.git;\
	cd ./toml;\
	python3 ./setup.py bdist_wheel;\
	$SUDO pip3 install --upgrade ./dist/*.whl;\
	cd ..;\
	rm -rf ./toml;\
	\
	git clone --depth=50 https://github.com/hukkin/tomli.git;\
	git clone --depth=50 https://github.com/pyparsing/pyparsing.git;\
	git clone --depth=50 https://github.com/takluyver/flit.git;\
	git clone --depth=50 https://github.com/pypa/packaging.git;\
	git clone --depth=50 https://github.com/pypa/pep517.git;\
	git clone --depth=50 https://github.com/pypa/build.git;\
	\
	cd tomli;\
	PYTHONPATH=.:../build/src:../packaging:../pyparsing:../flit/flit_core:../pep517 $SUDO python3 -m build -nwx .;\
	$SUDO pip3 install --upgrade ./dist/*.whl;\
	cd ..;\
	rm -rf ./tomli;\
	\
	cd ./pyparsing;\
	python3 ./setup.py bdist_wheel;\
	$SUDO pip3 install --upgrade ./dist/*.whl;\
	cd ..;\
	rm -rf ./pyparsing;\
	\
	cd flit/flit_core;\
	python3 -c "from flit_core import build_thyself;build_thyself.build_wheel('./')";\
	$SUDO pip3 install --upgrade ./*.whl;\
	cd ../..;\
	rm -rf ./flit;\
	\
	cd pep517;\
	PYTHONPATH=../build/src:../packaging python3 -m build -xnw .;\
	$SUDO pip3 install --upgrade ./dist/*.whl;\
	\
	cd ../packaging;\
	PYTHONPATH=../build/src python3 -m build -xnw .;\
	$SUDO pip3 install --upgrade ./dist/*.whl;\
	cd ..;\
	rm -rf ./packaging;\
	\
	cd ./build;\
	PYTHONPATH=./src python3 -m build -xnw .;\
	$SUDO pip3 install --upgrade ./dist/*.whl;\
	cd ..;\
	rm -rf ./pep517 ./build ./packaging;\
	\
	cd ./setuptools_scm;\
	python3 ./setup.py bdist_wheel;\
	$SUDO pip3 install --upgrade ./dist/*.whl;\
	cd ..;\
	rm -rf ./setuptools_scm;\
	\
	apt-get install -y libffi-dev python3-apt python3-gpg python3-lxml python3-bs4 python3-certifi;\
	\
	. ./installPythonPackagesFromGit.sh ;\
	\
	pip3 install --upgrade cffi@https://foss.heptapod.net/pypy/cffi/-/archive/branch/default/cffi-branch-default.tar.bz2;\
	python3 ./fix_python_modules_paths.py;\
	\
	apt-get autoremove --purge -y ;\
	apt-get clean && rm -rf /var/lib/apt/lists/* ;\
	rm -r ~/.cache/pip ;\
	rm -rf /tmp/*
