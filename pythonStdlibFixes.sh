export PYTHON_MODULES_DIR=$(python3 -c "import sysconfig; print(sysconfig.get_paths()['platstdlib']);");
echo PYTHON_MODULES_DIR $PYTHON_MODULES_DIR;
if $( python3 -c "import sys;sys.exit(int(not (sys.version_info < (3, 5)) ))" ); then
 curl -O https://raw.githubusercontent.com/python/cpython/3.6/Lib/typing.py;
 curl -O https://raw.githubusercontent.com/python/cpython/3.5/Lib/linecache.py;
 curl -O https://raw.githubusercontent.com/python/cpython/3.5/Lib/traceback.py;
 #curl -O https://raw.githubusercontent.com/python/cpython/3.5/Lib/importlib/abc.py;
 #curl -O https://raw.githubusercontent.com/python/cpython/3.5/Lib/importlib/_bootstrap_external.py;
 mv ./typing.py ./linecache.py ./traceback.py $PYTHON_MODULES_DIR/
fi;
if $( python3 -c "import sys;sys.exit(int(not (sys.version_info < (3, 6)) ))" ); then
curl -O https://raw.githubusercontent.com/python/cpython/3.7/Lib/enum.py;
  mv ./enum.py $PYTHON_MODULES_DIR/
fi;
if $( python3 -c "import sys;sys.exit(int(not (sys.version_info < (3, 8)) ))" ); then
  curl -O https://github.com/python/cpython/raw/3.8/Lib/multiprocessing/shared_memory.py;
  mv ./shared_memory.py $PYTHON_MODULES_DIR/multiprocessing/
fi;
